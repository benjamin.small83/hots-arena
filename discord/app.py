import discord
import asyncio
from discord.voice_client import VoiceClient
from discord import FFmpegPCMAudio
from discord import PCMVolumeTransformer
from general_config import GeneralConfig
from discord import opus

api_token = GeneralConfig("discord").get_setting("api_token")
if not api_token:
    raise Exception("Must load an API token in your config")

class Client(discord.Client):

    def get_channel_by_name(self, name):
        for guild in self.guilds:
            for channel in guild.channels:
                if channel.name != "General" and channel.type != "VoiceChat":
                    continue
                return channel
        return None

    async def on_ready(self):
        print(f'We have logged in as {self.user}', flush=True)
        print(f"Opus is loaded is {opus.is_loaded()}")
        channel = self.get_channel_by_name("General")
        voice_client = await channel.connect()
        voice_client.play(FFmpegPCMAudio("/app/resources/ba-dun-tss.mp3"))

    async def on_voice_state_update(self, member, before, after):
        pass

    async def on_message(self,message):

        if message.author == self.user:
            return

        if message.content.startswith('$inspect'):
            channel = self.get_channel_by_name("General")
            members = [f"{v.name}#{v.discriminator}" for v in channel.members]
            await message.channel.send('Users in general: ' + ",".join(members))



Client().run(api_token)
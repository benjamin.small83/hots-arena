import eventlet
eventlet.monkey_patch()

from flask import Flask
from flask import render_template
from flask_socketio import SocketIO
from flask_socketio import join_room
from flask_socketio import emit

app = Flask(__name__)

# initialize Flask
socketio = SocketIO(app, message_queue="amqp://guest:guest@rabbitmq.sys//")

@app.route('/')
def index():
    """Serve the index HTML"""
    return render_template('index.html')

@socketio.on('join')
def on_join(data):
    join_room('default')
    emit('default', {'message': 'joined default room'})

if __name__ == '__main__':
    socketio.run(app, host="0.0.0.0", debug=True)
